@RunWith(Cucumber.class)
Feature: User can Login into moodle,
  user can create a calendar entry,
  user can open a course,
  user can navigate to all navigationpages,
  user can log out

  Background: Login in Moodle
    When I go to Moodle
    And I enter my login credentials
    Then I am logged in Moodle

  Scenario: create calendar entry
    When I open the navigation menu
    And I click on the "CALENDER"
    And I see the "CALENDER" with "Kalender"
    And I click on new Entry Button
    And I type the appointment title
    And I click on save button
    And I click on Home
    Then I click on the side-menu and log out

  Scenario: open the first course
    When I open the first course link in the List
    And I see the announcement section
    And I click on Home
    Then I click on the side-menu and log out

  Scenario Outline: navigate to specific Page
    When I open the navigation menu
    And I click on the "<page>"
    And I see the "<page>" with "<text>"
    And I close the navigation menu
    And I click on Home
    Then I click on the side-menu and log out

    # Navigationpages
    Examples:
      | page              | text                       |
      | DASHBOARD         | Zuletzt besuchte Kurse     |
      | CALENDER          | Kalender                   |
      | KURS_AREA         | Kurse suchen               |
      | ALL_KURS_AREAS    | Allgemein                  |
      | IT_DOCUMMENTATION | IT-Dokumentation           |
      | TEACHING_INFO     | Informationen für Lehrende |
      | LIBRARY           | Bibliothek                 |
