@RunWith(Cucumber.class)
Feature: Login in Cis and search for persons

  Background: Login in Cis
    When I go to Cis
    And I click on Login
    Then I am on the cis page

  Scenario Outline: search and view the result
    When I type in the search "<searchValue>"
    And I press Enter
    And I click on the result
    Then I see a profile page and check the profile type "<expectedType>"

    Examples:
      | searchValue     | expectedType         |
      | Greil           | StudentIn     |
      | Michael Deutsch | MitarbeiterIn |
      | Brian Hatfield  | MitarbeiterIn Extern |
