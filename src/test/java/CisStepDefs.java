import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.net.URISyntaxException;
import java.util.Properties;

public class CisStepDefs {

  private WebDriver webDriver;

  @When("I go to Cis")
  public void iGoToCis() throws URISyntaxException {

    Properties prop = Utils.getConfig();
    //get username and password from config file
    String username = prop.getProperty("user");
    String password = prop.getProperty("password");

    String cisUrl = "https://" + username + ":" + password + "@cis.fh-vie.ac.at/cis/index_login.php";
    webDriver = SeleniumUtil.getWebDriver();
    webDriver.get(cisUrl);
  }

  @And("I click on Login")
  public void iClickOnLogin() {
    webDriver.findElement(By.className("cis_login")).click();
  }

  @Then("I am on the cis page")
  public void iAmOnTheCisPage() {
    try {
      webDriver.switchTo().frame("content");
      WebElement element = webDriver.findElement(By.xpath("/html/body/table/tbody/tr/td[1]/h1"));
      if (!"News".equals(element.getText())) {
        throw new AssertionError("Not on the News-site");
      }
      webDriver.switchTo().defaultContent();
    } catch (Exception e) {
      SeleniumUtil.quit();
      throw new AssertionError("Cis not displayed");
    }
  }

  @When("I type in the search {string}")
  public void iTypeInTheSearch(String searchValue) {
    webDriver.findElement(By.id("globalsearch")).sendKeys(searchValue);
  }

  @And("I press Enter")
  public void iPressEnter() {
    webDriver.findElement(By.id("globalsearch")).sendKeys(Keys.RETURN);
  }

  @And("I click on the result")
  public void iClickOnTheResult() {
    webDriver.switchTo().frame("content"); // Magic. Do not touch.
    webDriver.findElement(By.xpath("//html//body//table//tbody//tr[1]//td[3]//a")).click();
  }

  @Then("I see a profile page and check the profile type {string}")
  public void iSeeAProfilePage(String expectedType) {

    WebElement element = webDriver.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td[1]/table[1]/tbody/tr/td[2]/b[1]"));
    if (!element.getText().equals(expectedType)) {
      SeleniumUtil.quit();
      throw new AssertionError("Wrong Type");
    }
    //See the result for some seconds (just a demonstration)
    Utils.wait(2);
  }
}
