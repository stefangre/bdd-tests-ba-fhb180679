import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Utils {

  static Properties getConfig() {
    Properties prop = new Properties();
    try {
      InputStream input = Thread.currentThread()
          .getContextClassLoader()
          .getResourceAsStream("config.properties");

      if (input != null) {
        prop.load(input);
      }
    } catch (IOException ex) {
      SeleniumUtil.quit();
      ex.printStackTrace();
    }
    return prop;
  }

  static void wait(int timeout) {
    try {
      TimeUnit.SECONDS.sleep(timeout);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
