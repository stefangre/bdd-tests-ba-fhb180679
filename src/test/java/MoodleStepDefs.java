import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.MoodlePage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URISyntaxException;
import java.time.Duration;
import java.util.Date;
import java.util.Properties;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class MoodleStepDefs {

  Properties prop = Utils.getConfig();
  private WebDriver webDriver;
  private WebDriverWait wait;

  @When("I go to Moodle")
  public void iGoToMoodle() throws URISyntaxException {
    String moodleUrl = "https://moodle.fh-vie.ac.at/";
    webDriver = SeleniumUtil.getWebDriver();
    wait = new WebDriverWait(webDriver, Duration.ofSeconds(10).getSeconds());
    webDriver.get(moodleUrl);
  }

  @And("I enter my login credentials")
  public void iEnterMyLoginCredentials() {
    //get username and password from config file
    String username = prop.getProperty("user");
    String password = prop.getProperty("password");

    webDriver.findElement(By.id("login_username")).sendKeys(username);
    webDriver.findElement(By.id("login_password")).sendKeys(password + Keys.ENTER);
  }

  @Then("I am logged in Moodle")
  public void iAmLoggedInMoodle() {
    WebElement element = webDriver.findElement(By.cssSelector("#action-menu-toggle-1 > span > span.usertext.mr-1"));
    String name = prop.getProperty("name");
    if (!name.equals(element.getText())) {
      throw new AssertionError("Not logged-in in moodle");
    }
  }

  @When("I open the navigation menu")
  public void iOpenOnTheNavigationMenu() {
    String expanded = webDriver.findElement(By.xpath("//button[contains(@class, 'btn nav-link float-sm-left mr-1 btn-light bg-gray')]")).getAttribute("aria-expanded");
    if ("false".equals(expanded))
      toggleNavigationMenu();
  }

  @And("I close the navigation menu")
  public void iCloseOnTheNavigationMenu() {
    toggleNavigationMenu();
  }

  @And("I click on the {string}")
  public void iClickOnThePage(String page) {
    By navigationElement = MoodlePage.valueOf(page).getNavigationElement();
    wait.until(presenceOfElementLocated(navigationElement));
    webDriver.findElement(navigationElement).click();
  }

  @And("I see the {string} with {string}")
  public void iSeeCustomText(String page, String givenText) {
    By element = MoodlePage.valueOf(page).getElementToCheck();
    String actualText = getElementText(element);
    if (!givenText.equals(actualText)) {
      SeleniumUtil.quit();
      throw new AssertionError("Not on the Kalender Page");
    }
  }

  @And("I click on Home")
  public void iClickOnHome() {
    webDriver.findElement(By.cssSelector("#page-wrapper > nav > a")).click();
  }

  @Then("I click on the side-menu and log out")
  public void iLogOut() {
    By sideMenuXpath = By.xpath("//html/body/div[3]/nav/ul[2]/li[3]/div/div/div/div/div/a");
    wait.until(presenceOfElementLocated(sideMenuXpath));
    webDriver.findElement(sideMenuXpath).click();

    By logoutSelector = By.cssSelector("#actionmenuaction-6");
    wait.until(presenceOfElementLocated(logoutSelector));
    webDriver.findElement(logoutSelector).click();
  }

  @When("I open the first course link in the List")
  public void iOpenOnThefirstCourseLinkInTheList() {
    By firstCourse = By.xpath("//html/body/div[3]/div[3]/div/div/section[2]/aside/section[6]/div/div/ul/li[1]/div/a");
    wait.until(presenceOfElementLocated(firstCourse));
    webDriver.findElement(firstCourse).click();
  }

  @And("I see the announcement section")
  public void iSeeTheAnnouncementSection() {
    By announcementElement = By.xpath("//html/body/div[2]/div[3]/div/div/section/div/div/ul/li[1]/div[3]/ul/li/div/div/div[2]/div/a/span");
    wait.until(presenceOfElementLocated(announcementElement));
    String announcementElementText = webDriver.findElement(announcementElement).getText();

    if (!announcementElementText.contains("Ankündigungen")) {
      SeleniumUtil.quit();
      throw new AssertionError("Not on a Course Page");
    }
  }

  @And("I click on new Entry Button")
  public void iClickOnNewEntryButton() {
    By newEntryButton = By.xpath("//html/body/div[3]/div[3]/div/div/section[1]/div/div/div[1]/div/div[1]/button");
    wait.until(presenceOfElementLocated(newEntryButton));
    webDriver.findElement(newEntryButton).click();
  }

  @And("I type the appointment title")
  public void iTypeTheAppointmentTitle() {
    By nameInput = By.id("id_name");
    wait.until(presenceOfElementLocated(nameInput));
    webDriver.findElement(nameInput).sendKeys("Automatisierter Test: " + new Date().toGMTString());
    Utils.wait(1);
  }


  @And("I click on save button")
  public void iClickOnSaveButton() {
    By saveButton = By.xpath("/html/body/div[6]/div/div/div[3]/button");
    wait.until(presenceOfElementLocated(saveButton));
    webDriver.findElement(saveButton).click();
    Utils.wait(1);
  }


  private String getElementText(final By element) {
    wait.until(presenceOfElementLocated(element));
    return webDriver.findElement(element).getText();
  }

  private void toggleNavigationMenu() {
    webDriver.findElement(By.xpath("//button[contains(@class, 'btn nav-link float-sm-left mr-1 btn-light bg-gray')]")).click();
  }

}
