package models;

import lombok.*;
import org.openqa.selenium.By;

@Getter
@AllArgsConstructor
public enum MoodlePage {
  CALENDER(By.cssSelector("#nav-drawer > nav > ul > li:nth-child(2) > a > div > div > span.media-body"),
      By.xpath("//*[@id='page-header']/div/div/div/div[1]/div[1]/div/div/h1")),
  KURS_AREA(By.cssSelector("#nav-drawer > nav > ul > li:nth-child(3) > a > div > div > span.media-body"),
      By.xpath("//*[@id=\"coursesearch\"]/fieldset/label")),
  ALL_KURS_AREAS(By.cssSelector("#nav-drawer > nav > ul > li:nth-child(4) > a > div > div > span.media-body"),
      By.xpath("//*[@id='page-header']/div/div/div/div[1]/div[1]/div/div/h1")),
  IT_DOCUMMENTATION(By.cssSelector("#nav-drawer > nav > ul > li:nth-child(5) > a > div > div > span.media-body"),
      By.xpath("//*[@id='page-header']/div/div/div/div[1]/div[1]/div/div/h1")),
  TEACHING_INFO(By.cssSelector("#nav-drawer > nav > ul > li:nth-child(6) > a > div > div > span.media-body"),
      By.xpath("//*[@id='page-header']/div/div/div/div[1]/div[1]/div/div/h1")),
  LIBRARY(By.cssSelector("#nav-drawer > nav > ul > li:nth-child(7) > a > div > div > span.media-body"),
      By.xpath("//*[@id='page-header']/div/div/div/div[1]/div[1]/div/div/h1")),
  DASHBOARD(By.cssSelector("#nav-drawer > nav > ul > li:nth-child(1) > a > div > div > span.media-body"),
      By.xpath("/html/body/div[3]/div[3]/div/div/section[1]/div/aside/section[2]/div/h5"));
  private final By navigationElement;
  private final By elementToCheck;
}
