import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

class SeleniumUtil {

  private static WebDriver webDriver;

  static synchronized WebDriver getWebDriver() throws URISyntaxException {
    if (webDriver == null) {
      initialize();
    }
    return webDriver;
  }

  private static void initialize() throws URISyntaxException {
    URL resource = SeleniumUtil.class.getClassLoader().getResource("chromedriver.exe");
    System.setProperty("webdriver.chrome.driver", Paths.get(resource.toURI()).toFile().getAbsolutePath());
    webDriver = new ChromeDriver();
  }

  static void quit() {
    if (webDriver != null) {
      webDriver.quit();
    }
  }

}
