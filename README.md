# java-cucumber-example

Clone the repo:

```
git@gitlab.com:stefangre/bdd-tests-ba-fhb180679.git
```

## Config File

Fill the config file in 

    src/test/resources/config.properties 
    
to run the features.


## Run features

Navigate to feature directory.

    src\test\resources\cucumber\
  
Select one and press right mouse button and select:

    Run 'Feature XYZ'
    
    
## Chromedriver

Version in use: 89.0.4389.23

Local Chrome Browser is not allowed to have a lower version than the chromedriver.

If you need a other Version:
     
    https://chromedriver.chromium.org/downloads
    
kill all chromedriver processes in windows
    
    taskkill /F /IM chromedriver.exe
